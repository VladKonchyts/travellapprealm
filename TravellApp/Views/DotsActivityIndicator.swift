//
//  DotsActivityIndicator.swift
//  TravellApp
//
//  Created by macbook on 17.04.21.
//

import UIKit

class DotsActivityIndicator: UIView {
    
    private var dots: [UIView] = []
    
    @IBInspectable
    var dotsCount: Int = 4 {
        didSet {
            removeDots()
            configureDots()
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotRadius: CGFloat = 8 {
        didSet {
            for dot in dots {
                configureDotSize(dot)
                setNeedsLayout()
            }
        }
    }
    
    @IBInspectable
    var dotSpacing: CGFloat = 8

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureDots()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureDots()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let center = CGPoint(x: frame.width/2, y: frame.height/2)
                let middle = dots.count/2
                for i in 0..<dots.count {
                    let x = center.x + CGFloat(i - middle)*(dotRadius * 2 + dotSpacing)
                    let y = center.y
                    dots[i].frame.origin = CGPoint(x: x, y: y)
                }
    }
    
    private func configureDots() {
        for i in 0..<dotsCount {
            let dot = UIView()
            configureDotSize(dot)
            configureDotColor(dot)
            dots.append(dot)
            addSubview(dot)
        }
        startAnimation()
    }
    
    private func configureDotSize(_ dot: UIView) {
        dot.frame.size = CGSize(width: 2 * dotRadius, height: 2 * dotRadius)
        dot.layer.cornerRadius = dotRadius
    }
    
    private func configureDotColor(_ dot: UIView) {
        dot.backgroundColor = UIColor(named: "PurpleColor")
    }
    
    func startAnimation() {
        var delay: TimeInterval = 0
        for dot in dots {
            let animation = scaleAnimation(delay)
            dot.layer.add(animation, forKey: "scaleUpDown")
            delay += 0.2
        }
    }
    
    func stopAnimation() {
        removeDots()
    }
    
    private func removeDots() {
        for dot in dots {
            dot.removeFromSuperview()
        }
        dots.removeAll()
    }
    
    private func scaleAnimation(_ delay: TimeInterval) -> CAAnimationGroup {
        let scaleUp = CABasicAnimation(keyPath: "transform.scale")
        scaleUp.beginTime = delay
        scaleUp.fromValue = 1
        scaleUp.toValue = 1.5
        scaleUp.duration = 0.2
        
        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.beginTime = delay + scaleUp.duration
        scaleDown.fromValue = 1.5
        scaleDown.toValue = 1
        scaleDown.duration = 0.2
        
        let group = CAAnimationGroup()
        group.animations = [scaleUp, scaleDown]
        group.repeatCount = .infinity
        group.duration = (scaleUp.duration + scaleDown.duration) * TimeInterval(dots.count)
        return group
    }
}

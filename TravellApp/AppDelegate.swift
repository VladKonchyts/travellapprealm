//
//  AppDelegate.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        window = UIWindow(frame: UIScreen.main.bounds)
        let splashVC = SplashViewController()
        let navigationController = UINavigationController(rootViewController: splashVC)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }



}


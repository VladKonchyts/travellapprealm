//
//  RealmTravel.swift
//  TravellApp
//
//  Created by macbook on 10.04.21.
//
import UIKit
import RealmSwift

@objc enum Transport: Int, RealmEnum {
    case airplane, train, car, none
}

enum Currency: String {
    case none = ""
    case ruble = "₽"
    case euro = "€"
    case dollar = "$"
}

class RealmTravel: Object {
    @objc dynamic var id = ""
    @objc dynamic var userId = ""
    @objc dynamic var name = ""
    @objc dynamic var descript = ""
    let stops = List<RealmStop>()
    
    override class func primaryKey() -> String? {
        return #keyPath(RealmTravel.id)
    }
}



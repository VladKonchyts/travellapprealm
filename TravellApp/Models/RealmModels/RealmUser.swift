//
//  RealmUser.swift
//  TravellApp
//
//  Created by macbook on 8.05.21.
//

import UIKit
import RealmSwift

class RealmUser: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var nickName = ""
    
    override class func primaryKey() -> String? {
        return #keyPath(RealmUser.id)
    }
}

//
//  RealmStop.swift
//  TravellApp
//
//  Created by macbook on 27.04.21.
//

import UIKit
import RealmSwift

class RealmStop: Object {
    @objc dynamic var id = ""
    @objc dynamic var travelId = ""
    @objc dynamic var name = ""
    @objc dynamic var rating = 0
    @objc dynamic var spentMoney = 0
    @objc dynamic var latitude = 0
    @objc dynamic var longitude = 0
    @objc dynamic var descript = ""
    @objc dynamic var transport: Transport = .none
    @objc dynamic var spentMoneyCurrencyString: String = ""
    
    override class func primaryKey() -> String? {
        return #keyPath(RealmStop.id)
    }
}

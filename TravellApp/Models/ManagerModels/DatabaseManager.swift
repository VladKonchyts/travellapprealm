//
//  DatabaseManager.swift
//  TravellApp
//
//  Created by macbook on 27.04.21.
//

import Foundation
import RealmSwift
import Firebase
import FirebaseDatabase

class DatabaseManager {
    static let shared = DatabaseManager()
    func saveTravel(from array: [Any]) {
        for item in array {
            if let travelJson = item as? [String: Any] {
                if let id = travelJson["id"] as? String,
                   let name = travelJson["name"] as? String,
                   let description = travelJson["description"] as? String,
                   let userId = travelJson["userId"] as? String {
                    let travel = RealmTravel()
                    travel.id = id
                    travel.userId = userId
                    travel.name = name
                    travel.descript = description
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(travel, update: .all)
                    }
                }
            }
        }
    }
    
    func saveStop(from array: [Any]) {
        for item in array {
            if let stopJson = item as? [String: Any] {
                if let id = stopJson["id"] as? String,
                   let travelId = stopJson["travelId"] as? String,
                   let name = stopJson["name"] as? String,
                   let rating = stopJson["rating"] as? Int,
                   let spentMoney = stopJson["spentMoney"] as? Double,
                   let currency = stopJson["currency"] as? String,
                   let latitude = stopJson["latitude"] as? Double,
                   let longitude = stopJson["longitude"] as? Double,
                   let description = stopJson["description"] as? String,
                   let transportInt = stopJson["transport"] as? Int
                {
                    let stop = RealmStop()
                    stop.id = id
                    stop.travelId = travelId
                    stop.name = name
                    stop.descript = description
                    stop.rating = rating
                    stop.spentMoney = Int(spentMoney)
                    stop.spentMoneyCurrencyString = currency
                    stop.latitude = Int(latitude)
                    stop.longitude = Int(longitude)
                    stop.transport = Transport(rawValue: transportInt)!
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(stop, update: .all)
                    }
                }
            }
        }
    }
    
    func saveUser(from array: [Any]) {
        for item in array {
            if let userJson = item as? [String: Any] {
                if let id = userJson["id"] as? String,
                   let name = userJson["name"] as? String,
                   let nickName = userJson["nickName"] as? String
                {
                    let user = RealmUser()
                    user.id = id
                    user.name = name
                    user.nickName = nickName
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(user, update: .all)
                    }
                }
            }
        }
    }
}

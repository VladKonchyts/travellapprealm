//
//  SpentMoneyViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit

class SpentMoneyViewController: UIViewController {
    
    @IBOutlet weak var spentTextField: UITextField!
    @IBOutlet weak var spentLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var spentMoney: Double = 0
    var currency: Currency = .none
    
    var closure: ((Double, Currency) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let segmentedMainColor = UIColor(named: "PurpleColor")
        let segmentedBackgroundColor = UIColor(named: "WhiteColor")
        let titleTextAttributesBlack = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleTextAttributesWhite = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributesBlack, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributesWhite, for: .selected)
        doneButton.layer.cornerRadius = 10
        segmentedControl.layer.borderColor = segmentedMainColor?.cgColor
        segmentedControl.layer.borderWidth = 1
        segmentedControl.layer.backgroundColor = segmentedBackgroundColor?.cgColor
        spentTextField.text = "\(spentMoney)"
        
        switch currency {
        case .dollar:
            segmentedControl.selectedSegmentIndex = 0
        case .euro:
            segmentedControl.selectedSegmentIndex = 1
        case .ruble:
            segmentedControl.selectedSegmentIndex = 2
        case .none:
            break
        }
    }
    
    
    @IBAction func doneClicked(_ sender: Any) {
        if let text = spentTextField.text, let money = Double(text) {
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                currency = .dollar
            case 1:
                currency = .euro
            case 2:
                currency = .ruble
            default:
                break
            }
            closure?(money, currency)
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    
}

//
//  CreateStopViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import FirebaseDatabase
import RealmSwift


class CreateStopViewController: UIViewController, MapViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var segmentedControll: UISegmentedControl!
    @IBOutlet weak var currencyButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var stepperButton: UIStepper!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var spentMoneyCurencySegmentedClick: Currency = .none
    var money: Double = 0
    var stop: RealmStop?
    var transport: RealmStop?
    
    var latitudeMap: Double?
    var longitudeMap: Double?
    var travelId: String?
    var stopImage: [UIImage] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let stop = stop {
            ratingLabel.text = "\(Int(stop.rating))"
            nameTextField.text = stop.name
            spentMoneyLabel.text = "\(stop.spentMoneyCurrencyString)\(Int(stop.spentMoney))"
            descriptionTextView.text = stop.descript
            money = Double(stop.spentMoney)
            spentMoneyCurencySegmentedClick = Currency(rawValue: stop.spentMoneyCurrencyString)!
            locationLabel.text = "\(stop.latitude), \(stop.longitude)"
            
            switch stop.transport {
            case .airplane:
                segmentedControll.selectedSegmentIndex = 0
            case .train:
                segmentedControll.selectedSegmentIndex = 1
            case .car:
                segmentedControll.selectedSegmentIndex = 2
            case .none:
                break
            }
        }
        
        let segmentedMainColor = UIColor(named: "PurpleColor")
        let segmentedBackgroundColor = UIColor(named: "WhiteColor")
        
        segmentedControll.layer.borderColor = segmentedMainColor?.cgColor
        segmentedControll.layer.borderWidth = 1
        segmentedControll.layer.backgroundColor = segmentedBackgroundColor?.cgColor
        stepperButton.layer.borderWidth = 1
        stepperButton.layer.cornerRadius = 10
        stepperButton.layer.borderColor = segmentedMainColor?.cgColor
        stepperButton.layer.backgroundColor = segmentedBackgroundColor?.cgColor
        stepperButton.setDecrementImage(stepperButton.decrementImage(for: .normal), for: .normal)
        stepperButton.setIncrementImage(stepperButton.incrementImage(for: .normal), for: .normal)
        
        navigationItem.title =  "Остановка"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButton(_:)))
        
        collectionView.delegate = self
        collectionView.dataSource = self
        let xib = UINib(nibName: "StopImageColllection", bundle: nil)
        collectionView.register(xib, forCellWithReuseIdentifier: "StopImageColllection")
        collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
        navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func plusButton(_ sender: Any) {
        let mapVC = MapViewController()
        mapVC.delegate = self
        mapVC.latitude = latitudeMap
        mapVC.longitude = longitudeMap
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func currencyButton(_ sender: Any) {
        let spentVC = SpentMoneyViewController()
        present(spentVC, animated: true, completion: nil)
        
        spentVC.closure = { [weak self] money, currency in
            guard let self = self else { return }
            self.spentMoneyLabel.text = "\(currency.rawValue)\(Int(money))"
            self.money = money
            self.spentMoneyCurencySegmentedClick = currency
        }
    }
    
    @IBAction func stepperClicked(_ sender: Any) {
        ratingLabel.text = String(Int(stepperButton.value))
    }
    
    
    
    @objc func saveButton(_ sender: Any) {
        if let stop = stop {
            let realm = try! Realm()
            try! realm.write {
                stop.name = nameTextField.text ?? ""
                stop.rating = Int(ratingLabel.text ?? "") ?? 0
                stop.latitude = Int(latitudeMap ?? 0)
                stop.longitude = Int(longitudeMap ?? 0)
                stop.descript = descriptionTextView.text
                stop.spentMoney = Int(money)
                stop.spentMoneyCurrencyString = spentMoneyCurencySegmentedClick.rawValue
                stop.transport = Transport(rawValue: segmentedControll.selectedSegmentIndex)!
            }
        } else {
            stop = RealmStop()
            stop!.id = UUID().uuidString
            stop!.travelId = travelId ?? ""
            stop!.name = nameTextField.text ?? ""
            stop!.rating = Int(ratingLabel.text ?? "") ?? 0
            stop!.latitude = Int(latitudeMap ?? 0)
            stop!.longitude = Int(longitudeMap ?? 0)
            stop!.descript = descriptionTextView.text
            stop!.spentMoney = Int(money)
            stop!.spentMoneyCurrencyString = spentMoneyCurencySegmentedClick.rawValue
            stop!.transport = Transport(rawValue: segmentedControll.selectedSegmentIndex)!
            if segmentedControll.selectedSegmentIndex == 0 {
                stop!.transport = .airplane
            } else if segmentedControll.selectedSegmentIndex == 1 {
                stop!.transport = .train
            } else if segmentedControll.selectedSegmentIndex == 2 {
                stop!.transport = .car
            }
            let realm = try! Realm()
            try! realm.write {
                realm.add(stop!, update: .all)
            }
        }
        
        let json: [String: Any] = ["id": stop!.id ,
                                   "travelId": travelId ?? 0,
                                   "name": stop!.name ,
                                   "rating": stop!.rating ,
                                   "spentMoney": stop!.spentMoney ,
                                   "currency": stop!.spentMoneyCurrencyString ,
                                   "latitude": stop!.latitude ,
                                   "longitude": stop!.longitude  ,
                                   "description": stop!.descript ,
                                   "transport": stop!.transport.rawValue ]
        let database = Database.database().reference()
        let child = database.child("stops").child("\(stop!.id)")
        child.setValue(json) { (error, ref) in
            
        }
        navigationController?.popViewController(animated: true)
    }
    
    func mapControllerSelectLocation(latitude: Double, longitude: Double) {
        locationLabel.text = "\(latitude), \(longitude)"
        self.latitudeMap = Double(Int(latitude))
        self.longitudeMap = Double(Int(longitude))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 65, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stopImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StopImageColllection", for: indexPath) as! StopImageColllection
        cell.stopImage.image = stopImage[indexPath.row]
        return cell
    }
    
    @IBAction func addImageButtonCkicked(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Saved Photos", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.openCameraRoll()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            stopImage.append(pickedImage)
            collectionView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func openCameraRoll(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access saved photos.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

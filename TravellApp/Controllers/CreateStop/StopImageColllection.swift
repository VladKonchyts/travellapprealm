//
//  StopImageColllection.swift
//  TravellApp
//
//  Created by macbook on 2.04.21.
//

import UIKit

class StopImageColllection: UICollectionViewCell {

    @IBOutlet weak var stopImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        stopImage.layer.cornerRadius = 10
        stopImage.layer.borderWidth = 5.0
        stopImage.layer.borderColor = UIColor(named: "ProfileSeparatorColor")?.cgColor
        stopImage.contentMode = .scaleToFill
    }
    
}

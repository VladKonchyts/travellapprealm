//
//  MapViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerSelectLocation(latitude: Double, longitude: Double)
}

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var latitude: Double?
    var longitude: Double?
    var delegate: MapViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let longTapRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longTap(sender:)))
        mapView.addGestureRecognizer(longTapRecognizer)
        
        if let latitude = latitude, let longitude = longitude {
           let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.addAnnotation(annotation)
        }
    }
    
    @objc func longTap(sender: UIGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("possible")
        case .began:
            mapView.removeAnnotations(mapView.annotations)
            let locationInView = sender.location(in: mapView)
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationOnMap
            mapView.addAnnotation(annotation)
            delegate?.mapControllerSelectLocation(latitude: locationOnMap.latitude, longitude: locationOnMap.longitude)
        case .changed:
            print("changed")
        case .ended:
            print("ended")
        case .cancelled:
            print("cancelled")
        case .failed:
            print("failed")
        @unknown default:
            print("default")
        }
    }

}

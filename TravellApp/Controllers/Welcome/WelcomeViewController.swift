//
//  WelcomeViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import Firebase
import FirebaseRemoteConfig

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var createAccButton: UIButton!
    @IBOutlet weak var loginEmailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createAccButton.layer.cornerRadius = 10
        loginEmailButton.layer.cornerRadius = 10
        let remoteConfig = RemoteConfig.remoteConfig()
        let loginText = remoteConfig["login_button"].stringValue
        loginEmailButton.setTitle(loginText, for: .normal)
        let registerText = remoteConfig["register_button"].stringValue
        createAccButton.setTitle(registerText, for: .normal)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func createAccountButton(_ sender: Any) {
        let registrationVC = RegistrationViewController()
        navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    @IBAction func loginEmailButton(_ sender: Any) {
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
}

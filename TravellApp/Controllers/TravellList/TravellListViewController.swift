//
//  TravellListViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import Firebase
import FirebaseDatabase
import RealmSwift
import SwipeCellKit

class TravellListViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var travelTableView: UITableView!
    
    // MARK: - Properties
    
    let user = Auth.auth().currentUser
    var travels: Results<RealmTravel>!
    var filterTravels: Results<RealmTravel>!
    let searchController = UISearchController(searchResultsController: nil)
    var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    var travelNotificationToken: NotificationToken?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Путешествия"
        print("Realm file path: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
        
        let realm = try! Realm()
        travels = realm.objects(RealmTravel.self)
        travelNotificationToken = travels.observe { [weak self] (changes) in
            guard let self = self else { return }
            switch changes {
            case .initial(_):
                break
            case .update(_, let deletions, let insertions, let modifications):
                self.travelTableView.reloadData()
                break
            case .error(_):
                break
            }
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCustomButton(_:)))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Shape"), style: .done, target: self, action: #selector(profileCustomButton(_:)))
        travelTableView.delegate = self
        travelTableView.dataSource = self
        let xib = UINib(nibName: "TravelListCell", bundle: nil)
        travelTableView.register(xib, forCellReuseIdentifier: "TravelListCell")
        travelTableView.tableFooterView = UIView()
        travelTableView.reloadData()
        
        downloadAndParseTravels {
            for travel in self.travels {
                self.downloadStops(for: travel) {
                    self.travelTableView.reloadData()
                }
            }
        }
        downloadUsers {
        }
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Travels"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = false
        travelTableView.reloadData()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        travelTableView.reloadData()
    }
    
    // MARK: - Methods
    
    func downloadStops(for travel: RealmTravel, onComplete: (() -> ())? ) { 
        ServerManager.shared.downloadStops(for: travel.id) { (arrayOfDictionaries) in
            DatabaseManager.shared.saveStop(from: arrayOfDictionaries)
            onComplete?()
        }
    }
    
    func downloadAndParseTravels(onComplete: (() -> ())? ) {
        let userId = Auth.auth().currentUser?.uid ?? ""
        ServerManager.shared.downloadTravels(for: userId) { (arrayOfDictionaries) in
            DatabaseManager.shared.saveTravel(from: arrayOfDictionaries)
            onComplete?()
        }
    }
    
    func downloadUsers(onComplete: (() -> ())? ) {
        let id = Auth.auth().currentUser?.uid ?? ""
        ServerManager.shared.downloadUsers(for: id) { (arrayOfDictionaries) in
            DatabaseManager.shared.saveUser(from: arrayOfDictionaries)
            onComplete?()
        }
    }

    @objc func profileCustomButton(_ sender: Any) {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    // MARK: - IBActions
    
    @IBAction func addCustomButton(_ sender: Any) {
        
        let alertVC = UIAlertController(title: "Добавить новое путешествие", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            if let userId = Auth.auth().currentUser?.uid {
                let travel = RealmTravel()
                travel.id = UUID().uuidString
                travel.userId = userId
                travel.name = alertVC.textFields![0].text!
                travel.descript = alertVC.textFields![1].text!
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(travel)
                }
                let json = ["id": travel.id,
                            "userId": travel.userId,
                            "name": travel.name,
                            "description": travel.descript]
                let database = Database.database().reference()
                let child = database.child("travels").child("\(travel.id)")
                child.setValue(json) { (error, ref) in
                }
            }
        }
        alertVC.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertVC.addAction(cancelAction)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Ведите название путешествия ..."
            textField.textColor = .black
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Ведите описание путешествия ..."
            textField.textColor = .black
        }
        
        present(alertVC, animated: true, completion: nil)
        
    }
}

extension TravellListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filterTravels.count
        }
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TravelListCell") as! TravelListCell
        var travel = travels[indexPath.row]
        if isFiltering {
            travel = filterTravels[indexPath.row]
        } else {
            travel = travels[indexPath.row]
        }
        cell.setup(with: travel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopListVC = StopListViewController()
        stopListVC.travel = travels[indexPath.row]
        if isFiltering {
            stopListVC.travel = filterTravels[indexPath.row]
        } else {
            stopListVC.travel = travels[indexPath.row]
        }
        navigationController?.pushViewController(stopListVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, nil) in
            guard let self = self else { return }
            let travel = self.travels[indexPath.row]
            let travelsId = travel.id
            let realm = try! Realm()
            try! realm.write {
                let stop = realm.objects(RealmStop.self).filter("travelId == %@", travelsId)
                realm.delete(stop)
                realm.delete(travel)
            }
            self.travelTableView.deleteRows(at: [indexPath], with: .automatic)
            self.travelTableView.reloadData()
        }
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
}

extension TravellListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text ?? "")
    }
    func filterContentForSearchText(_ searchText: String) {
        filterTravels = travels.filter("name CONTAINS[c] '\(searchText)'")
        travelTableView.reloadData()
    }
}

//
//  TravelListCell.swift
//  TravellApp
//
//  Created by macbook on 23.02.21.
//

import UIKit
import RealmSwift

class TravelListCell: UITableViewCell {

    @IBOutlet weak var firstTravellStar: UIImageView!
    @IBOutlet weak var secondTravellStar: UIImageView!
    @IBOutlet weak var thirdTravellStar: UIImageView!
    @IBOutlet weak var fourthTravellStar: UIImageView!
    @IBOutlet weak var fifthTravellStar: UIImageView!
    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelDescriptionLabel: UILabel!
    
    var travel: RealmTravel! {
        didSet {
            travelNameLabel.text = travel.name
            travelDescriptionLabel.text = travel.descript
            firstTravellStar.image = UIImage(named: "Star Icon2")
            secondTravellStar.image = UIImage(named: "Star Icon2")
            thirdTravellStar.image = UIImage(named: "Star Icon2")
            fourthTravellStar.image = UIImage(named: "Star Icon2")
            fifthTravellStar.image = UIImage(named: "Star Icon2")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func getRating(for travel: RealmTravel?) -> Int {
        
        let realm = try! Realm()
        let stops = realm.objects(RealmStop.self).filter("travelId == %@", travel?.id)
        var sum = 0
        for stop in stops {
            if stop == stop {
                sum += stop.rating
            }
            return sum / stops.count
        }
        return 0
    }

    func setup(with travel: RealmTravel?) {
        travelNameLabel.text = travel?.name ?? ""
        travelDescriptionLabel.text = travel?.descript ?? "" 
        firstTravellStar.image = UIImage(named: "Star Icon2")
        secondTravellStar.image = UIImage(named: "Star Icon2")
        thirdTravellStar.image = UIImage(named: "Star Icon2")
        fourthTravellStar.image = UIImage(named: "Star Icon2")
        fifthTravellStar.image = UIImage(named: "Star Icon2")
        if travel == travel {
            
            let rating = getRating(for: travel!)
            
            switch rating {
            case 1:
                firstTravellStar.image = UIImage(named: "Star Icon")
            case 2:
                firstTravellStar.image = UIImage(named: "Star Icon")
                secondTravellStar.image = UIImage(named: "Star Icon")
            case 3:
                firstTravellStar.image = UIImage(named: "Star Icon")
                secondTravellStar.image = UIImage(named: "Star Icon")
                thirdTravellStar.image = UIImage(named: "Star Icon")
            case 4:
                firstTravellStar.image = UIImage(named: "Star Icon")
                secondTravellStar.image = UIImage(named: "Star Icon")
                thirdTravellStar.image = UIImage(named: "Star Icon")
                fourthTravellStar.image = UIImage(named: "Star Icon")
            case 5:
                firstTravellStar.image = UIImage(named: "Star Icon")
                secondTravellStar.image = UIImage(named: "Star Icon")
                thirdTravellStar.image = UIImage(named: "Star Icon")
                fourthTravellStar.image = UIImage(named: "Star Icon")
                fifthTravellStar.image = UIImage(named: "Star Icon")
            default:
                print("0")
            }
        }
        
    }
    
}

//
//  ForgotViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit

class ForgotViewController: UIViewController {

    @IBOutlet weak var resetButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetButton.layer.cornerRadius = 10
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}

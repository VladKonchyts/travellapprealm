//
//  LoginViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    var isNeedToShowPassword = true
    
    @IBOutlet weak var hiddenEyeButton: UIButton!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var loginButton: UIButton!
    
    @IBOutlet var separatorEmail: UILabel!
    @IBOutlet var separatorPassword: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 10
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] (user, error) in
                if user != nil {
                    let travellListVC = TravellListViewController()
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(travellListVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    guard let self = self else { return }
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
            
    }
    
    @IBAction func eyeButton(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
        if isNeedToShowPassword == false {
            hiddenEyeButton.setImage(UIImage(named:"hide"), for: .normal)
            isNeedToShowPassword = true
        } else {
            hiddenEyeButton.setImage(UIImage(named:"View"), for: .normal)
            isNeedToShowPassword = false
        }
    }
    @IBAction func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotClicked(_ sender: Any) {
        let forgotVC = ForgotViewController()
        navigationController?.pushViewController(forgotVC, animated: true)
    }
    
}

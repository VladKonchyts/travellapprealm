//
//  StopListViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import RealmSwift

class StopListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var stopListTableView: UITableView!
    var stops: Results<RealmStop>?
    var stopsNotificationToken: NotificationToken?
    var travel: RealmTravel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        stops = realm.objects(RealmStop.self).filter("travelId == %@", travel!.id)
        stopsNotificationToken = stops?.observe { [weak self] (changes) in
            guard let self = self else { return }
            switch changes {
            case .initial(_):
                self.stopListTableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                self.stopListTableView.reloadData()
                break
            case .error(_):
                break
            }
        }
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButton(_:)))
        title = travel?.name
        stopListTableView.delegate = self
        stopListTableView.dataSource = self
        let xib = UINib(nibName: "StopListCell", bundle: nil)
        stopListTableView.register(xib, forCellReuseIdentifier: "StopListCell")
        stopListTableView.tableFooterView = UIView()
        stopListTableView.reloadData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 131
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stops?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopListCell") as! StopListCell
        let stop = stops![indexPath.row]
        cell.stopNameLabel.text = stop.name
        cell.stopDescriptionLabel.text = stop.descript
        cell.stopCurrencyLabel.text = "\(stop.spentMoneyCurrencyString)\(Int(stop.spentMoney))"
        cell.firstStar.image = UIImage(named: "Star Icon2")
        cell.secondStar.image = UIImage(named: "Star Icon2")
        cell.thirdStar.image = UIImage(named: "Star Icon2")
        cell.fourthStar.image = UIImage(named: "Star Icon2")
        cell.fifthStar.image = UIImage(named: "Star Icon2")
        
        switch stop.rating {
        case 1:
            cell.firstStar.image = UIImage(named: "Star Icon")
        case 2:
            cell.firstStar.image = UIImage(named: "Star Icon")
            cell.secondStar.image = UIImage(named: "Star Icon")
        case 3:
            cell.firstStar.image = UIImage(named: "Star Icon")
            cell.secondStar.image = UIImage(named: "Star Icon")
            cell.thirdStar.image = UIImage(named: "Star Icon")
        case 4:
            cell.firstStar.image = UIImage(named: "Star Icon")
            cell.secondStar.image = UIImage(named: "Star Icon")
            cell.thirdStar.image = UIImage(named: "Star Icon")
            cell.fourthStar.image = UIImage(named: "Star Icon")
        case 5:
            cell.firstStar.image = UIImage(named: "Star Icon")
            cell.secondStar.image = UIImage(named: "Star Icon")
            cell.thirdStar.image = UIImage(named: "Star Icon")
            cell.fourthStar.image = UIImage(named: "Star Icon")
            cell.fifthStar.image = UIImage(named: "Star Icon")
        default:
            print("0")
        }
        
        switch stop.transport {
        
        case .airplane:
            cell.transportImgeView.image = UIImage(named: "Airplane")
        case .train:
            cell.transportImgeView.image = UIImage(named: "Train")
        case .car:
            cell.transportImgeView.image = UIImage(named: "Car")
        case .none:
            break
        }
        
        return cell
    }
    
    @IBAction func addButton(_ sender: Any) {
        let createStopVC = CreateStopViewController()
        createStopVC.travelId = travel.id
        navigationController?.pushViewController(createStopVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let createStopVC = CreateStopViewController()
        createStopVC.travelId = travel.id
        createStopVC.stop = stops![indexPath.row]
        navigationController?.pushViewController(createStopVC, animated: true)
    }
    
}

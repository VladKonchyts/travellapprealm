//
//  StopListCell.swift
//  TravellApp
//
//  Created by macbook on 26.02.21.
//

import UIKit

class StopListCell: UITableViewCell {

    @IBOutlet weak var stopNameLabel: UILabel!
    @IBOutlet weak var stopDescriptionLabel: UILabel!
    @IBOutlet weak var stopCurrencyLabel: UILabel!
    @IBOutlet var transportImgeView: UIImageView!
    
    @IBOutlet weak var firstStar: UIImageView!
    @IBOutlet weak var secondStar: UIImageView!
    @IBOutlet weak var thirdStar: UIImageView!
    @IBOutlet weak var fourthStar: UIImageView!
    @IBOutlet weak var fifthStar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}

//
//  ProfileViewController.swift
//  TravellApp
//
//  Created by macbook on 30.03.21.
//

import UIKit
import Firebase
import FirebaseDatabase
import RealmSwift

class ProfileViewController: UIViewController, ImagePickerDelegate {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    
    var avatarImage: UIImage?
    var imagePicker: ImagePicker!
    var user: Results<RealmUser>?
    var userInfo: RealmUser?
    var userNotificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        avatarImageView?.layer.cornerRadius = (avatarImageView?.frame.size.width ?? 0.0) / 2
        avatarImageView?.layer.borderWidth = 3.0
        avatarImageView?.layer.borderColor = UIColor.white.cgColor
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        let realm = try! Realm()
        user = realm.objects(RealmUser.self)
        userNotificationToken = user?.observe { [weak self] (changes) in
            guard let self = self else { return }
            switch changes {
            case .initial(_):
                break
            case .update(_, let deletions, let insertions, let modifications):
                self.nameLabel.text = self.userInfo?.name
                self.nickNameLabel.text = self.userInfo?.nickName
                break
            case .error(_):
                break
            }
        }
        
        let realms = try! Realm()
        userInfo = realms.objects(RealmUser.self).last
        nameLabel.text = userInfo?.name
        nickNameLabel.text = userInfo?.nickName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func nameNickNameCkicked(_ sender: Any) {
        let alertVC = UIAlertController(title: "Профиль", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [self] (action) in
            if let id = Auth.auth().currentUser?.uid {
                let user = RealmUser()
                user.id = id
                user.name = alertVC.textFields?[0].text ?? ""
                user.nickName = alertVC.textFields?[1].text ?? ""
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(user)
                }
                let json = ["id": user.id,
                            "name": user.name,
                            "nickName": user.nickName]
                let database = Database.database().reference()
                let child = database.child("user").child("\(user.id)")
                child.setValue(json) { (error, ref) in
                }
                nameLabel.text = user.name
                nickNameLabel.text = user.nickName
            }
        }
        alertVC.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertVC.addAction(cancelAction)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Ведите имя ..."
            textField.textColor = .black
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Ведите никнейм ..."
            textField.textColor = .black
        }
        
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func exitClicked(_ sender: Any) {
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [UIViewController];
        for aViewController in viewControllers {
            if(aViewController is WelcomeViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    
    @IBAction func customBackClicked(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func aboutClicked(_ sender: Any) {
        let version : Any! = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let build : Any! = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
        let alertVC = UIAlertController(title: nil, message: "Version \(version!)\n Build \(build!)", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertVC.addAction(action)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func imagePickerClicked(_ sender: Any) {
        self.imagePicker.present(from: sender as! UIView)
    }
    
    func didSelect(image: UIImage?) {
        self.avatarImageView.image = image
        
    }
}

//
//  RegistrationViewController.swift
//  TravellApp
//
//  Created by macbook on 9.02.21.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController {
    
    
    @IBOutlet weak var hiddenEyeButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    
    
    var isNeedToShowPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registrationButton.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func registrationButton(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] (user, error) in
                if user != nil {
                    let travellListVC = TravellListViewController()
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(travellListVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    guard let self = self else { return }
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func eyeClicked(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
        if isNeedToShowPassword == false {
            hiddenEyeButton.setImage(UIImage(named:"hide"), for: .normal)
            isNeedToShowPassword = true
        } else {
            hiddenEyeButton.setImage(UIImage(named:"View"), for: .normal)
            isNeedToShowPassword = false
        }
    }
    @IBAction func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

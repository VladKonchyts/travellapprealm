//
//  SplashViewController.swift
//  TravellApp
//
//  Created by macbook on 30.03.21.
//

import UIKit
import Firebase
import FirebaseRemoteConfig

class SplashViewController: UIViewController {

    @IBOutlet weak var dotsActivityIndicator: DotsActivityIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        RemoteConfig.remoteConfig().fetchAndActivate { [weak self] (status, error) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let welcomeVC = WelcomeViewController()
                self.navigationController?.pushViewController(welcomeVC, animated: true)
            }
        }
    }
}


